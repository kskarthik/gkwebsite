# GNUKhata website

## Building 
Build requirements
- Hugo
- Git

Instructions

```bash
$ git clone https://gitlab.com/kskarthik/gkwebsite.git
$ cd gkwebsite
$ hugo server -D
```
Open localhost:1313 in your browser to preview the website

## Licensing

This Theme is released under [Creative Commons Attribution 3.0 (CC-BY-3.0) License](https://creativecommons.org/licenses/by/3.0/)
